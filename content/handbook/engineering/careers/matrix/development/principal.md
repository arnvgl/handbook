---
title: "Development Department Career Framework: Principal"
---

## Development Department Competencies: Principal

{{% include "includes/engineering/dev-career-matrix-nav.md" %}}

**Principals at GitLab are expected to exhibit the following competencies:**

- [Principal Leadership Competencies](#principal-leadership-competencies)
- [Principal Technical Competencies](#principal-technical-competencies)
- [Principal Values Alignment](#principal-values-alignment)

---

### Principal Leadership Competencies

{{% include "includes/engineering/principal-leadership-competency.md" %}}
{{% include "includes/engineering/development-principal-leadership-competency.md" %}}
  
### Principal Technical Competencies

{{% include "includes/engineering/principal-technical-competency.md" %}}
{{% include "includes/engineering/development-principal-technical-competency.md" %}}

### Principal Values Alignment

{{% include "includes/engineering/values-competency.md" %}}
{{% include "includes/engineering/development-principal-values-competency.md" %}}

