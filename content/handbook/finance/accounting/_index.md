---
title: "Accounting and Reporting"
---

This page contains GitLab's accounting and reporting policies, which can be made public. Please find our internal processes in the [Accounting and Reporting](https://internal.gitlab.com/handbook/finance/accounting/) internal handbook section.

## Foreign Currency Translation Policy

**Overview**

Foreign currency translation describes the method used in converting a foreign entity's functional currency (as determined and documented in GitLab.com>Finance>Issues>#630) to the reporting entity's financial statement currency. Prior to translating the foreign entity’s financial statements into the reporting entity’s currency, the foreign entity’s financials must be prepared in accordance with generally accepted accounting principles (GAAP), specifically under [Financial Accounting Standards Board (FASB) Statement No.52](https://www.fasb.org/page/PageContent?pageId=/reference-library/superseded-standards/summary-of-statement-no-52.html). GitLab’s financial statement reporting currency is USD. The functional currency of our non-U.S. subsidiaries is the local currency. Changes in foreign currency translation are recorded in other comprehensive income (loss), which is reported in the consolidated statement of equity and ultimately carried over to the consolidated balance sheet, under equity.

**Exchange Rates**

Exchange rates used in the currency translation process vary across the three primary financial statement components:

- **Assets and Liabilities:**  Exchange rate between functional currency and reporting currency at period-end.
- **Income Statement:**  The average exchange rates during the period presented.
- **Equity:** The historical exchange rate at the date when entry is made to shareholder's equity. Changes in retained earnings are based on historical exchange rates of each period's income statement.

**Transaction Risk vs Translation Risk**

**Currency Transaction Risk**

Currency transaction risk is due to company transactions denominated in foreign currencies. These transactions must be restated into the entity functional currency equivalents before they can be recorded. Gains(losses) are recognized when a payment is made or interim balance sheet dates.

**Currency Translation Risk**

Currency translation risk occurs due to the company owning assets and liabilities denominated in a foreign currency.

**Cumulative Translation Adjustment**

A cumulative translation adjustment (CTA) is an entry to the comprehensive income section of a translated balance sheet that summarizes the gains(losses) resulting from exchange rate differences over time. Currency values shift constantly, affecting how a currency is valued against others. The CTA is a line item in the consolidated balance sheet that captures gains(losses) associated with international business activity and exposure to foreign markets. The changes in CTA are recorded in other comprehensive income (loss). CTA’s are required under GAAP since they help distinguish between actual operating gains(losses) and those that arise from the currency translation process. Additional information on our reporting standards surrounding CTA's can be found in [FASB Topic 830, "Foreign Currency Matters."](https://fasb.org/Page/ShowPdf?path=ASU2013-05.pdf)

Recording CTA - Exchange rate gains and losses for individual transactions are captured automatically by our ERP system, NetSuite. However, a CTA entry must be made in order to properly distinguish currency translation gains(losses) from other general gains(losses) in the consolidated financial statements. This entry includes reconciliation of any inter-company activity that generates foreign exchange gains(losses). The CTA is made on a monthly basis as part of our financial statement reporting cycle.

## Chart of Accounts Policy

**Scope**

This policy establishes GitLab’s guidelines regarding the structure, responsibilities and requirements underlying the [chart of accounts (COA).](https://www.investopedia.com/terms/c/chart-accounts.asp)

**Purpose**

This policy establishes formal responsibilities and accountabilities for how GitLab handles requests for new, modified or closed data elements on the COA. The Controller is responsible for all aspects of financial accounting and reporting, and governs the COA.  All requests for new or modified (including closure/deactivation) COA segments, hierarchies, and configuration attributes are subject to approval by the Finance team.

**Changes to the COA**

All requests for new or modified accounts must be submitted to the Accounting Manager for review and approval through a request using the Finance issue tracker.

There are other stakeholders associated with the COA that may influence certain business decisions or financial system configurations. The Controller and Accounting Manager will include selected stakeholders in the related procedures and processes when and if appropriate. Potential stakeholders include, but may not be limited to:

- Financial Planning and Analysis
- Data & Analytics
- Other departments who have shared functionalities within the financial system

The general ledger attributes subject to this policy will be defined by the Controller based upon factors including but not limited to:

- Creating and maintaining consistency for the structure of accounts
- Standard accounting policies and practices
- Regulatory compliance requirements and reporting needs
- Financial and operational reporting needs and requirements
- External accounting and financial reporting requirements

Once an amendment to the COA has been approved, the Accounting Manager will ensure the necessary changes are implemented by updating and then closing the issue.

**Administration**

The COA is maintained in NetSuite. Changes to the COA can only be made by the Controller and/or Accounting Manager.

## Account Reconciliation Policy

**Scope**

This policy applies to GitLab Inc. (“GitLab” or the “Company”) and all of its subsidiaries.

**Purpose**

To establish guidelines for assessing, preparing and reviewing balance sheet account reconciliations on a consistent basis in accordance with corporate policies and US Generally Accepted Accounting Principles (“GAAP”).

**Policy**

Account reconciliations are prepared and reviewed monthly or quarterly for each active balance sheet account at the natural account level based upon the risk rating assessed (see risk rating assessment below).  Account reconciliations will be prepared consolidated in USD or by entity in the respective functional currency.

Each month end close the Accounting Manager assigns each balance sheet account or groups of accounts to its respective preparer and reviewer using FloQast (Account Reconciliation Tool).  The assignments are set once and will roll over into the next accounting period.  The Accounting Manager will make changes to assignments as needed.  The preparer and reviewers can not be the same person to ensure segregation of duties.

The balances from NetSuite will be auto synced into FloQast each period end so the preparers can prepare their recons based on the NetSuite ending balance for their respective assigned accounts.

The preparer(s) will ensure the following:

- Review all activities ensure activity is recorded properly.
- Identify any reconciling items.
- Upload any supporting documentation and/or add schedules as needed.
- Sign-off the recon in FloQast.

The reviewer(s) will ensure the following:

- Ensures account reconciliations are prepared in compliance with this policy.
- Ensures the account reconciliations are complete, accurate and appropriate.
- Sign-off the account reconciliation in FloQast which will update the status of the recon to the reviewed/approved status.

FloQast will auto sign-off the recon on our behalf if the following is met:

- Balance is zero

If the balance changes after review, approval or auto sign-off the recon will be automatically unreconciled by FloQast and the preparer and reviewers will need to follow the above steps again.

**Risk Rating Assessment**

Once a year in the beginning of Q4, the Controller and/or CFO will review each active balance sheet account and rate it from High, Medium and Low.  The risk level of each account is evaluated based both on the quantitative value (to determine materiality) and the qualitative factors listed below:

- Level of judgement required:  risk increases as level of judgement required increases
- routine/non-routine transactions: risk increases as amount of non-routine/non-standard processes required to record the activity increases
- History of issues: risk increases as the number of audit-related adjustments, questions, restatements increases.
- Complexity: risk increases due to business/system changes, new pronouncements, and new complex calculations that impact the balance sheet.

High Risk Accounts will be reconciled by the preparer monthly (for the exception of tax and equity related accounts which will be reconciled quarterly) and will require 1st level review by an accounting manager or above and 2nd level review by the CFO or PAO.

Medium Risk Accounts will be reconciled by the preparer monthly and will require 1st level review by an accounting manager or above.

Low Risk Accounts will be reconciled by the preparer monthly or quarterly and will require 1st level review by an accounting manager or above.

If there is no activity and/or the account balance is zero the reconciliation will be auto certified by BlackLine.

Once each reconciliation is reviewed/approved the account reconciliation is locked in BlackLine and no further changes can be made for that period.

**Completeness Check**

Once the period is officially closed the Senior Accounting Manager will ensure all recons are in approved, reviewed or in a auto-certified status before moving into the next period.

